<footer class="address">
    <section class="newsletter" id="contato">
        <div id="box-texto">
            <div id="texto">
            <h1>Assine nossa Newsletter</h1>
            <i>promoções e eventos mensais</i>
        </div> 
        <input type="email" name="email" id="email-texto" placeholder="Digite seu email">
        <button id="email-botao">Enviar</button>
        </div>
        
    </section>
    <div id="endereco"><p>Praia de Botafogo, 300, 5º andar - Botafogo - Rio de Janeiro</p>
        <img src="<?php echo get_stylesheet_directory_uri(); ?>/img/brafe.png" alt="brafe logo"></div>
        
    </footer>

   <?php wp_footer(); ?> 
</body>
</html>