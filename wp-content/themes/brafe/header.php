<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>
        <?php
        if (is_front_page()){
            echo wp_title("");
        } else if (is_page()){
            echo wp_title(); echo '-';
        } else if (is_search()) {
            echo 'Busca -';
        } elseif (!(is_404()) && (is_single()) || (is_page())) {
            echo wp_title(); echo '-';
        } elseif (is_404()){
            echo 'Não encontrada -';
        }
        bloginfo( "name" )
        ?>
    </title>
    <?php 
                wp_head();
                ?>  
    
</head>
<body>
    <header class="menu">        
            <a id="logo-link" href="<?php if(is_front_page()){ echo "#";}else {echo get_home_url();}?>"><img src="<?php echo get_stylesheet_directory_uri();?>/img/brafe.png" alt="logo brafé">
            <!---else {echo get_home_url();} ---->
            
            <?php 
    
                $args = array(
                'menu' => 'principal', 
                'theme_location' => 'navegacao',
                'container' => false
                     );
            
                wp_nav_menu($args)
            ?>


           <!--- <ul>
                <li><a href="/sobre">Sobre</a></li>
                <li><a href="/produtos">Produtos</a></li>
                <li><a href="/portfolio">Portfólio</a></li>
                <li><a href="/contato">Contato</a></li>
            </ul>    ---->
    </header>