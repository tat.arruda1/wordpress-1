<?php 

// Template Name: Home Page

?>

<?php get_header(); ?>

<section class="ini">

<div>
<img src="<?php echo get_stylesheet_directory_uri(); ?>/img/bg-intro.jpg" alt="background logo">
<div id="relative">
<p><strong> <?php the_field('titulo_chamativo') ?></strong></p>
<i> <?php the_field('subtitulo_chamativo') ?></i></div>

</section>
<section class="apres" id="sobre">
<h1><?php the_field('titulo_sobre') ?></h1>
<div id="box">
    <figure id="cafe-1"><img src=" <?php the_field('primeira_imagem') ?> " alt="xícara de café com desenho">
    <figcaption><?php the_field('legenda_primeira_foto') ?></figcaption> </figure>
    
    <figure id="cafe-2"><img src="<?php the_field('segunda_imagem') ?>" alt="xícara de café simples">
    <figcaption><?php the_field('legenda_segunda_foto') ?></figcaption> </figure>
    
</div>

<p>
    <i><?php the_field('texto_sobre') ?></i> 
</p>   
</section>
<section class="tipos" id="produtos">

<div id="principal">
 
    <div>
    <div id="bola-marrom-maior">
    <div id="bola-marrom"></div>
    </div>
    
    <h1><?php the_field('origem_do_cafe_1') ?></h1>
    <p><?php the_field('origem_texto_1') ?></p>
    </div>
    <div>
    <div id="bola-laranja-maior">
        <div id="bola-laranja"></div>
    </div>
    
    <h1><?php the_field('origem_do_cafe_2') ?></h1>
    <p><?php the_field('origem_texto_2') ?></p>
    </div>
    <div>
    <div id="bola-amarela-maior">
         <div id="bola-amarela"></div>
    </div>
   
    <h1><?php the_field('origem_do_cafe_3') ?></h1>
    <p><?php the_field('origem_texto_3') ?></p>
    </div>
    </div>

<div id="botao">
    <a href="">SAIBA MAIS</a>
</div>
</section>
<section class="bairros" id="portfolio">
<div id="bloco-bairro">
    <img src="<?php the_field('foto_da_loja_1') ?>" alt="Botafogo">
    <div id="info-bairro"><h1><?php the_field('local_da_loja_1') ?></h1>
    <p> <?php the_field('descricao_da_loja_1') ?></p>
    <a href="">Ver Mapa</a>
    </div>   
</div>
<div id="bloco-bairro">
    <img src="<?php the_field('foto_da_loja_2') ?>" alt="Iguatemi">
    <div id="info-bairro">
        <h1><?php the_field('local_da_loja_2') ?></h1>
        <p><?php the_field('descricao_da_loja_2') ?></p>
        <a href="">Ver Mapa</a>
    </div>

</div>
<div id="bloco-bairro">
    <img src="<?php the_field('foto_da_loja_3')?>" alt="Mineirão">
    <div id="info-bairro">
    <h1><?php the_field('local_da_loja_3') ?></h1>
    <p><?php the_field('descricao_da_loja_3') ?></p>
    <a href="">Ver Mapa</a></div>
    
</div>
</section>
<?php get_footer(); ?>