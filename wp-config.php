<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'IMuioQffy33ntlVJttyRL5+phR8N9ki94REOTVmDCNORc1/zHftJ2/KlqES95x3D5d+eiuUwdP5n9HEOUaSXQg==');
define('SECURE_AUTH_KEY',  '0P9bKi9EB9oC5+kPmMX384HUT0uM2sy5azQ/3tkrD68P5sguO0KWfEix6G5DggL8ATtO1Y/oeHWcVGfRIvq/Bg==');
define('LOGGED_IN_KEY',    '2863gPAGfnU5J4M4doaoWXO/0Lhja1/T3HAYKNOJ8lpFqukVCOpuRtOEzNQszneM2CMhASe86UOndk28blt0ww==');
define('NONCE_KEY',        'oJOL/7Yoaqp1CRA99zOgDUf/rKUp5ojv1MyqFbPY85na57InQSzuY8lXtkrEE3CNsjZK8SqPoseuCjt4ArLTRA==');
define('AUTH_SALT',        'eOpHUqVvuiFZpQkyYLk3XU7QOGWqTqSPys7ErmWDwvxaKJcMalHLxb6+3P0OkZs10llAFzWgfCZpFhljMO5muA==');
define('SECURE_AUTH_SALT', 'Ld5TT1kj7esvnR7SQGb9DECxkoHYxhjGwqWSKZU+lGsmnLEmQ42PTXPeCBtQIlbVbah+DVMngWQWPbVDoCK6dw==');
define('LOGGED_IN_SALT',   '2Ese1zn09wsJBi+zXVMo9WTChWYk1KBljH3l9JCjqReA8ekH2UvTh0sZlAh1dpGKeZD6WelVy7Oix/360i2zoA==');
define('NONCE_SALT',       'd4EQpyjja8B/gNjeBYXyYKzVyY5LFsSPBbBdTpdcNvX/L5wH+hn+tHPwuTxR+X07i0myI8eCqzsvhvGgdu8spw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
